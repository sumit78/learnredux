import {createStore,combineReducers} from 'redux';

const student = (state=[],action) => {
  switch (action.type){
    case "STUDENT_DATA" :{
      return [...state,action.data];
    }
    default:{
      return state;
    }
  }
};

const product = (state = [],action)=>{
  switch(action.type){
    case "PRODUCT_DATA":{
      return [...state,action.data];
    }
    default:{
      return state;
    }
  }
}

const rootReducer = combineReducers({
  Student:student,
  Product:product
}) 

const store = createStore(rootReducer);

const insertStudent = (data) =>{
return {
  type:"STUDENT_DATA",
  data,
}
}

const insertProduct = (data) =>{
  return {
    type:"PRODUCT_DATA",
    data,
  }
  }

  store.subscribe(()=>console.log(store.getData));

  store.dispatch(insertStudent({
    name:'sumit',
    Age:"25",
  }))
  store.dispatch(insertProduct({
    name:'pepsodent',
    Rate:"50",
  }))
import React from 'react'
import { createStore } from 'redux'
import Counter from './components/Counter'
import counter from './reducers/index'

import './App.css';

const store = createStore(counter);

function App() {
  return (
    <div>
  <Counter
   
    value={store.getState()}
    onIncrement={() => store.dispatch({ type: 'INCREMENT' })}
    onDecrement={() => store.dispatch({ type: 'DECREMENT' })}
  />
    </div>
  );
}

export default App;
